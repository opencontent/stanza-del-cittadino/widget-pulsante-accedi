import {
  Button,
  Dropdown,
  DropdownMenu,
  DropdownToggle,
  Icon,
  LinkList,
  LinkListItem,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader
} from 'design-react-kit';
import { Fragment, useEffect, useState } from 'react';
import { authActions, currentUserActions } from '../_store';
import { useDispatch, useSelector } from 'react-redux';
import { t } from 'i18next';
import styled from 'styled-components';
import jwtDecode from 'jwt-decode';

export { Login };

function Login() {
  const [openFirst, toggleFirst] = useState(false);
  const [isOpen, toggleModal] = useState(false);

  const dispatch = useDispatch();
  const { user_data, loading } = useSelector((x) => x.auth);
  const { currentUser } = useSelector((x) => x.currentUser);
  const { error } = useSelector((x) => x.auth);
  const decodeUserToken = (token) => {
    return jwtDecode(token);
  };

  // recovery token otherwise I create it
  useEffect(() => {
    if (process.env.NODE_ENV === 'development') {
      if (`${process.env.REACT_APP_DEV_TOKEN}` !== '' && `${process.env.REACT_APP_DEV_TOKEN}` !== 'undefined') {
        const user = decodeUserToken(`${process.env.REACT_APP_DEV_TOKEN}`);
        dispatch(authActions.storeUser(user));
      } else {
        dispatch(authActions.getSessionToken());
      }
    } else if (process.env.NODE_ENV === 'production') {
      dispatch(authActions.getSessionToken());
    }
  }, [dispatch]);

  // If auth user get data user
  useEffect(() => {
    if (user_data?.id) {
      if (process.env.NODE_ENV === 'development') {
        dispatch(
          currentUserActions.storeCurrentUser({
            id: 'b8068368-bb51-4bca-b0aa-e7b0048b3d2d',
            role: 'user',
            nome: 'Michelangelo',
            cognome: 'Buonarroti',
            full_name: 'Michelangelo Buonarroti',
            email: 'michelangelo.buonarroti@opencontent.it',
            created_at: '2024-10-21T17:47:43+02:00',
            updated_at: '2025-01-15T14:48:48+01:00',
            codice_fiscale: 'BNRMHL75C06G702B',
            data_nascita: '1975-03-06T11:00:40+01:00',
            luogo_nascita: 'Pisa',
            codice_nascita: 'G702',
            provincia_nascita: 'PI',
            stato_nascita: null,
            sesso: 'M',
            telefono: '2313213112',
            cellulare: '3491234567',
            indirizzo_domicilio: 'Piazza della Signoria 50100 Firenze',
            civico_domicilio: null,
            cap_domicilio: null,
            citta_domicilio: null,
            provincia_domicilio: null,
            stato_domicilio: null,
            indirizzo_residenza: 'via roma',
            civico_residenza: '',
            cap_residenza: '38122',
            citta_residenza: 'trento',
            provincia_residenza: 'TN',
            stato_residenza: '',
            spid_code: 'SPID-019'
          })
        );
      } else if (process.env.NODE_ENV === 'production') {
        dispatch(currentUserActions.getUserById({ id: user_data?.id }));
      }
    }
  }, [user_data, dispatch]);

  const openModal = () => {
    toggleModal(!isOpen);
    if (isOpen === false) {
      window.location.hash = 'login-box';
    } else {
      window.location.hash = '';
    }
  };

  useEffect(() => {
    if (window.location.hash === '#login-box' && isOpen === false) {
      openModal(!isOpen);
    }
  }, [window.location]);

  return (
    <ResponsiveContainer>
      <div className={'responsiveStyle'}>
        {currentUser?.nome === 'Anonymous' || !user_data.hasOwnProperty('id') ? (
          <>
            {window.OC_SPID_BUTTON === 'true' ? (
              <Button
                tag={'a'}
                color="primary"
                icon
                href={`${window.OC_AUTH_URL}?return-url=${location.href}`}
                data-element="personal-area-login"
                aria-label={t('login')}
              >
                <span className="rounded-icon">
                  <Icon color="primary" icon="it-user" aria-hidden={true} role={'presentation'} />
                </span>
                <span className="d-none d-lg-block">
                  <span className={'responsiveStyle'}>{t('login')}</span>
                </span>
              </Button>
            ) : (
              <Button
                tag={'button'}
                color="primary"
                icon
                data-element="personal-area-login"
                onClick={() => openModal(!isOpen)}
                role={'button'}
                aria-label={t('login')}
              >
                <span className="rounded-icon">
                  <Icon color="primary" icon="it-user" aria-hidden={true} role={'presentation'} />
                </span>
                <span className="d-none d-lg-block">
                  <span className={'responsiveStyle'}>{t('login')}</span>
                </span>
              </Button>
            )}
          </>
        ) : (
          <div className="docs-show-dropdown-open">
            <Dropdown isOpen={openFirst} toggle={() => toggleFirst(!openFirst)}  >
              <DropdownToggle
                color="primary"
                caret
                className={'btn btn-primary btn-icon'}
              >
                <span className="rounded-icon">
                  <Icon
                    color="primary"
                    icon="it-user"
                    style={{ marginLeft: 0 }}
                    aria-hidden={true}
                  />
                </span>
                <span className="d-none d-lg-block">
                  {currentUser?.nome && currentUser?.cognome
                    ? currentUser?.nome + '  ' + currentUser?.cognome
                    : t('login')}
                </span>
{/*              {openFirst ? (
                  <span className="d-none d-lg-block ms-1">
                    <Icon icon="it-collapse" color={'light'} aria-hidden={true} />
                  </span>
                ) : (
                  <span className="d-none d-lg-block ms-1">
                    <Icon icon="it-expand" color={'light'} aria-hidden={true} />
                  </span>
                )}*/}
              </DropdownToggle>
              <DropdownMenu>
                {window.OC_USER_MENU_DISABLED === undefined ||
                window.OC_USER_MENU_DISABLED === false ? (
                  <LinkList>
                    <LinkListItem
                      className="left-icon"
                      href={`${window.OC_BASE_URL}/it/user/profile`}
                      inDropdown
                    >
                      <Icon
                        className="left me-1"
                        color="primary"
                        icon="it-user"
                        aria-hidden={true}
                        size="sm"
                      />
                      <span>Profilo</span>
                    </LinkListItem>
                    <LinkListItem className="left-icon" href={`${window.OC_BASE_URL}/it/user`} inDropdown>
                      <Icon
                        className="left me-1"
                        color="primary"
                        icon="it-pa"
                        aria-hidden={true}
                        size="sm"
                      />
                      <span>{t('my_room')}</span>
                    </LinkListItem>
                    <LinkListItem className="left-icon" href={`${window.OC_BASE_URL}/it/pratiche`} inDropdown>
                      <Icon
                        className="left me-1"
                        color="primary"
                        icon="it-files"
                        aria-hidden={true}
                        size="sm"
                      />
                      <span>{t('my_applications')}</span>
                    </LinkListItem>
                    <LinkListItem
                      className="left-icon"
                      href={`${window.OC_BASE_URL}/it/pratiche/allegati`}
                      inDropdown
                    >
                      <Icon
                        className="left me-1"
                        color="primary"
                        icon="it-box"
                        aria-hidden={true}
                        size="sm"
                      />
                      <span>{t('my_attachments')}</span>
                    </LinkListItem>
                    <LinkListItem className="left-icon" href={`${window.OC_BASE_URL}/it/documenti`}
                                  inDropdown>
                      <Icon
                        className="left me-1"
                        color="primary"
                        icon="it-folder"
                        aria-hidden={true}
                        size="sm"
                      />
                      <span>{t('my_documents')}</span>
                    </LinkListItem>
                    <LinkListItem divider="1"></LinkListItem>
                    <LinkListItem className="left-icon" href={`${window.OC_BASE_URL}/logout`}
                                  inDropdown>
                      <Icon
                        className="left me-1"
                        color="primary"
                        icon="it-external-link"
                        aria-hidden={true}
                        size="sm"
                      />
                      <span>Logout</span>
                    </LinkListItem>
                  </LinkList>
                ) : (
                  <LinkList>
                    <LinkListItem className="left-icon" href={`${window.OC_BASE_URL}/logout`} inDropdown>
                      <Icon
                        className="left me-1"
                        color="primary"
                        icon="it-external-link"
                        aria-hidden={true}
                        size="sm"
                      />
                      <span>Logout</span>
                    </LinkListItem>
                  </LinkList>
                )}
              </DropdownMenu>
            </Dropdown>
          </div>
        )}

        <div>
          <Modal
            isOpen={isOpen}
            toggle={() => openModal(!isOpen)}
            centered
            labelledBy="buttonSpid"
            size={'lg'}
          >
            <ModalHeader toggle={() => openModal(!isOpen)} id="buttonSpid"></ModalHeader>
            <ModalBody>
              <div className="row">
                <div className="col-12 col-lg-8 offset-lg-2">
                    {error ? (
                        <div className="d-flex align-items-center justify-content-center">
                            <div>
                                <h3>{t("Servizio momentaneamente non disponibile!")}</h3>
                                <p>
                                    {t("Ci scusiamo per il disagio e vi invitiamo a riprovare in seguito.")}
                                </p>
                            </div>
                        </div>
                    ) : (   <div className="mt-3">
                        <h3 className="title-xlarge mb-2">{`${window.OC_AUTH_LABEL ? t('generic_subititle_text') : 'SPID'}`}</h3>
                        <div className="text-wrapper">
                            <p className="subtitle-small mb-3">{`${window.OC_AUTH_LABEL ? t('can_access') + ' ' + window.OC_AUTH_LABEL : t('spid_text')}`}</p>
                        </div>
                        <div className="button-wrapper mb-2">
                            <Button
                                tag={'a'}
                                href={`${window.OC_AUTH_URL}?return-url=${location.href}`}
                                type="button"
                                className="btn-icon btn-re square justify-content-center"
                                color={'primary'}
                                style={{ width: 300 }}
                            >
                                <img
                                    src={
                                        'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTExLjk5OTYgMEM1LjM4MzE0IDAgMCA1LjM4MzQzIDAgMTEuOTk5OEMwIDE4LjYxNjYgNS4zODMxNCAyNCAxMS45OTk2IDI0QzE4LjYxNjUgMjQgMjQgMTguNjE2NiAyNCAxMS45OTk4QzI0IDUuMzgzNDMgMTguNjE2NSAwIDExLjk5OTYgMFoiIGZpbGw9IndoaXRlIi8+CjxwYXRoIGQ9Ik05LjEzODI0IDExLjg0NDVDOC4zNzkyNSAxMS4wNzQ1IDggMTAuMTE4MiA4IDguOTc4NzlDOCA3LjgzNzEyIDguMzc5MjYgNi44ODc3MiA5LjEzMzQ5IDYuMTMxOTlDOS44ODcyNSA1LjM3ODU2IDEwLjg0ODIgNSAxMi4wMjgzIDVDMTMuMjAzNiA1IDE0LjE2MDMgNS4zODM2MyAxNC44OTUgNi4xNjEwNEMxNS42MzEyIDYuOTM3OTggMTYgNy44ODkyMiAxNiA5LjAyODU5QzE2IDEwLjE2NDcgMTUuNjMxMiAxMS4xMDg2IDE0Ljg5NSAxMS44NjYyQzE0LjE2MDMgMTIuNjIxIDEzLjIwOTMgMTMgMTIuMDM3OCAxM0MxMC44NjYzIDEzIDkuOTAwMDggMTIuNjE1NCA5LjEzODI0IDExLjg0NDVaIiBmaWxsPSIjMDA2QkQ2IiBzdHJva2U9IiMwMDZCRDYiIHN0cm9rZS13aWR0aD0iMC4zIi8+CjxwYXRoIGQ9Ik04IDE5QzggMTcuODUyIDguMzc1NTMgMTYuODk4MyA5LjExOTA2IDE2LjE0MTVDOS44Njg3MSAxNS4zODEgMTAuODIwNyAxNSAxMS45OTI5IDE1QzEzLjE1NTMgMTUgMTQuMTA0NSAxNS4zODU2IDE0LjgyNzMgMTYuMTY2QzE1LjU1NjIgMTYuOTQ3OSAxNiAxNy44NTU4IDE2IDE5SDhaIiBmaWxsPSIjMDA2QkQ2IiBzdHJva2U9IiMwMDZCRDYiIHN0cm9rZS13aWR0aD0iMC4zIi8+Cjwvc3ZnPgo='
                                    }
                                    alt="Entra con SPID"
                                    className="me-2"
                                />
                                <span className="">{`${window.OC_AUTH_LABEL ? t('log in') : t('enter_spid')}`}</span>
                            </Button>
                        </div>
                        {window.OC_AUTH_LABEL ? (
                            ''
                        ) : (
                            <a
                                className="simple-link"
                                target="_blank"
                                href="https://www.spid.gov.it/cos-e-spid/come-attivare-spid/"
                            >
                                {t('how_active_spid')}{' '}
                                <span className="visually-hidden">{t('how_active_spid')}</span>
                            </a>
                        )}
                    </div>)}

                </div>
              </div>
            </ModalBody>
            <ModalFooter className="mb-4"></ModalFooter>
          </Modal>
        </div>
      </div>
    </ResponsiveContainer>
  );
}

export const ResponsiveContainer = styled.div`
  @media (min-width: 992px) {
    .responsiveStyle {
      min-width: 248px;
    }
  }
`;
