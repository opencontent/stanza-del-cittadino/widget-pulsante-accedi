import { useEffect } from 'react';
import { loadFonts } from 'bootstrap-italia';
import { Login } from './pages/Login';

export function App() {
  useEffect(() => {
    // Importing files depending on env
    if (process.env.REACT_APP_STYLE === 'true') {
      import(`./assets/stylesheets/core.scss`);
      loadFonts('https://static.opencityitalia.it/fonts');
    }
  }, []);

  return <Login></Login>;
}
