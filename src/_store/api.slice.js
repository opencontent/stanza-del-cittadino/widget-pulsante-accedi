import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import { fetchWrapper } from '../_helpers/fetch-wrapper';

// create slice
const name = 'api';
const initialState = createInitialState();
const extraActions = createExtraActions();
const slice = createSlice({
  name,
  initialState,
  extraReducers: (builder) => {
    builder

      /** getApplicationById **/
      .addCase(`${name}/getApplicationById/fulfilled`, (state, action) => {
        state.application = action.payload;
      })
      .addCase(`${name}/getApplicationById/rejected`, (state, action) => {
        state.application = { error: action.error };
      })

      /** getServices **/
      .addCase(`${name}/getServices/pending`, (state, { meta }) => {
        state.services = { loading: true };
      })
      .addCase(`${name}/getServices/fulfilled`, (state, action) => {
        state.services = action.payload;
      })
      .addCase(`${name}/getServices/rejected`, (state, action) => {
        state.services = { error: action.error };
      })

      /** getCategories **/
      .addCase(`${name}/getCategories/pending`, (state, { meta }) => {
        state.categories = { loading: true };
      })
      .addCase(`${name}/getCategories/fulfilled`, (state, action) => {
        state.categories = action.payload;
      })
      .addCase(`${name}/getCategories/rejected`, (state, action) => {
        state.categories = { error: action.error };
      })

      /** createApplication **/
      .addCase(`${name}/createApplication/fulfilled`, (state, action) => {
        state.application = action.payload;
      })
      .addCase(`${name}/createApplication/rejected`, (state, action) => {
        state.application = { error: action.error };
      })

      /** getCategoryById **/
      .addCase(`${name}/getCategoryById/fulfilled`, (state, action) => {})

      /** getServicesByIdCategory **/
      .addCase(`${name}/getServicesByIdCategory/fulfilled`, (state, action) => {
        state.services = action.payload;
      })
      .addCase(`${name}/getServicesByIdCategory/rejected`, (state, action) => {
        state.services = { error: action.error };
      })
      /** getServiceHelpDesk **/
      .addCase(`${name}/getServiceHelpDesk/fulfilled`, (state, action) => {
        state.service_id = action.payload;
      })
      .addCase(`${name}/getServiceHelpDesk/rejected`, (state, action) => {
        state.service_id = { error: action.error };
      })

      /** getTenantInfo **/
      .addCase(`${name}/getTenantInfo/fulfilled`, (state, action) => {
        state.tenant_info = action.payload;
      })
      .addCase(`${name}/getTenantInfo/rejected`, (state, action) => {
        state.tenant_info = { error: action.error };
      })

      /** getDraftApplication **/
      .addCase(`${name}/getDraftApplication/fulfilled`, (state, action) => {
        state.application = action.payload;
      })
      .addCase(`${name}/getDraftApplication/rejected`, (state, action) => {
        state.application = { error: action.error };
      });
  }
});

// exports
export const apiActions = { ...slice.actions, ...extraActions };
export const apiReducer = slice.reducer;

// implementation

function createInitialState() {
  return {
    categories: [],
    services: [],
    application: {},
    service_id: '',
    tenant_info: {}
  };
}

function createExtraActions() {
  const baseUrl = window.OC_SUPPORT_APIURL || `${process.env.REACT_APP_API_URL}`;

  return {
    getApplicationById: getApplicationById(),
    getCategories: getCategories(),
    getCategoryById: getCategoryById(),
    createApplication: createApplication(),
    getServices: getServices(),
    getServicesByIdCategory: getServicesByIdCategory(),
    getServiceHelpDesk: getServiceHelpDesk(),
    getTenantInfo: getTenantInfo(),
    getDraftApplication: getDraftApplication()
  };

  function getApplicationById() {
    return createAsyncThunk(
      `${name}/getApplicationById`,
      async (id) => await fetchWrapper.get(`${baseUrl}/api/applications/${id}`)
    );
  }

  function getTenantInfo() {
    return createAsyncThunk(
      `${name}/getTenantInfo`,
      async (id) => await fetchWrapper.get(`${baseUrl}/api/tenants/info`)
    );
  }

  function getServices() {
    return createAsyncThunk(
      `${name}/getServices`,
      async () => await fetchWrapper.get(`${baseUrl}/api/services`)
    );
  }

  function getServiceHelpDesk() {
    return createAsyncThunk(
      `${name}/getServiceHelpDesk`,
      async () => await fetchWrapper.get(`${baseUrl}/api/services/helpdesk`)
    );
  }

  function getCategoryById() {
    return createAsyncThunk(
      `${name}/getCategoryById`,
      async (arg) => await fetchWrapper.get(`${baseUrl}/api/categories/${arg}`)
    );
  }

  function getServicesByIdCategory() {
    return createAsyncThunk(
      `${name}/getServicesByIdCategory`,
      async (arg) => await fetchWrapper.get(`${baseUrl}/api/services?topics_id=${arg}`)
    );
  }

  function getCategories() {
    return createAsyncThunk(
      `${name}/getCategories`,
      async (arg) => await fetchWrapper.get(`${baseUrl}/api/categories?not_empty=true`)
    );
  }

  function createApplication() {
    return createAsyncThunk(
      `${name}/createApplication`,
      async (data) => await fetchWrapper.post(`${baseUrl}/api/applications`, data)
    );
  }

  function getDraftApplication() {
    return createAsyncThunk(
      `${name}/getDraftApplication`,
      async (data) => await fetchWrapper.get(`${baseUrl}/api/applications?status=1000`, data)
    );
  }
}
