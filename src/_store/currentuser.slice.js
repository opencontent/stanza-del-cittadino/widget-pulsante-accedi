import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { fetchWrapper } from '../_helpers/fetch-wrapper';

// create slice

const name = 'currentUser';
const initialState = createInitialState();
const extraActions = createExtraActions();
const slice = createSlice({
  name,
  initialState,
  reducers: {
    storeCurrentUser: (state, action) => {
      const data = action.payload;
      state.currentUser = data;
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(`${name}/getCurrentUser/fulfilled`, (state, action) => {
        state.currentUser = action.payload;
      })
      .addCase(`${name}/getUserById/fulfilled`, (state, action) => {
        state.currentUser = action.payload;
      });
  }
});

// exports
export const currentUserActions = { ...slice.actions, ...extraActions };
export const currentUserReducer = slice.reducer;

// implementation
function createInitialState() {
  return {
    currentUser: {}
  };
}

function createExtraActions() {
  const baseUrl = window.OC_BASE_URL || `${process.env.REACT_APP_API_URL}`;

  return {
    getCurrentUser: getCurrentUser(),
    getUserById: getUserById()
  };

  function getCurrentUser() {
    return createAsyncThunk(
      `${name}/getCurrentUser`,
      async () => await fetchWrapper.get(`${baseUrl}/api/users`)
    );
  }

  function getUserById() {
    return createAsyncThunk(
      `${name}/getUserById`,
      async (arg) => await fetchWrapper.get(`${baseUrl}/api/users/${arg.id}`)
    );
  }
}

export const { storeCurrentUser } = slice.actions;
