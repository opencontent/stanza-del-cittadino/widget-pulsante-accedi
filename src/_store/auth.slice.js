import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import jwtDecode from 'jwt-decode';
import { fetchWrapper } from '../_helpers/fetch-wrapper';

// create slice
const name = 'auth';
const initialState = createInitialState();
const extraActions = createExtraActions();
const slice = createSlice({
  name,
  initialState,
  reducers: {
    logout: (state, action) => {
      state.token = null;
    },
    storeUser: (state, action) => {
      const data = action.payload;
      state.user_data = data;
    },
    error: (state, action) => {
      state.error = action.payload;
      state.loading = false;
    }
  },
  extraReducers: (builder) => {
    builder
      /** getSessionToken **/
      .addCase(`${name}/getSessionToken/pending`, (state, action) => {
        state.loading = true;
      })
      .addCase(`${name}/getSessionToken/fulfilled`, (state, action) => {
        const data = action.payload;
        state.user_data = jwtDecode(data.token);
        state.token = data.token;
        state.loading = false;
      })
      .addCase(`${name}/getSessionToken/rejected`, (state, action) => {
        state.loading = false;
      })
      /** createSessionToken **/
      .addCase(`${name}/createSessionToken/fulfilled`, (state, action) => {
        const data = action.payload;
        state.user_data = jwtDecode(data.token);
        state.token = data.token;
      });
  }
});

// exports
export const authActions = { ...slice.actions, ...extraActions };
export const authReducer = slice.reducer;

// implementation

function createInitialState() {
  return {
    // initialize state from local storage to enable user to stay logged in
    token: null,
    user_data: {},
    loading: false,
    error: false
  };
}

function createExtraActions() {
  const baseUrl = window.OC_BASE_URL || `${process.env.REACT_APP_API_URL}`;

  return {
    getSessionToken: getSessionToken(),
    createSessionToken: createSessionToken()
  };

  function getSessionToken() {
    return createAsyncThunk(
      `${name}/getSessionToken`,
      async () =>
        await fetchWrapper.getWithCredentials(`${baseUrl}/api/session-auth?with-cookie=true`)
    );
  }

  function createSessionToken() {
    return createAsyncThunk(
      `${name}/createSessionToken`,
      async () => await fetchWrapper.post(`${baseUrl}/api/session-auth`, {})
    );
  }
}

export const { logout, storeUser, error } = slice.actions;
