import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import detectLanguageCustom from './_helpers/detectLanguage';
import en from './i18n/en.json'
import it from './i18n/it.json'
import de from './i18n/de.json'
const languageDetector = new LanguageDetector();
languageDetector.addDetector(detectLanguageCustom);
// the translations
// (tip move them in a JSON file and import them,
// or even better, manage them separated from your code: https://react.i18next.com/guides/multiple-translation-files)

const resources = {
  en: {
    translation: en.login
  },
  it: {
    translation: it.login
  },
  de: {
    translation: de.login
  }
};

i18n
  .use(languageDetector)
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    fallbackLng: 'it',
    interpolation: {
      escapeValue: false // react already safes from xss
    },
    detection: {
      htmlTag: document.documentElement,
      // order and from where user language should be detected
      order: ['custom', 'cookie', 'htmlTag', 'localStorage'],
      // keys or params to lookup language from
      lookupCookie: 'i18next',
      lookupLocalStorage: 'i18next',
      lookupSessionStorage: 'i18next'
    }
  });

export default i18n;
