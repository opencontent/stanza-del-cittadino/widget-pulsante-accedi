import { authActions, store } from '../_store/index';

export const fetchWrapper = {
  get: request('GET'),
  getWithCredentials: requestWithCredentials('GET'),
  getGeoJson: request('GET', 'application/vnd.geo+json'),
  post: request('POST'),
  put: request('PUT'),
  delete: request('DELETE')
};

function request(method, type) {
  return (url, body) => {
    const requestOptions = {
      method,
      headers: authHeader(url, type)
    };
    if (body) {
      requestOptions.headers['Content-Type'] = type ? type : 'application/json';
      requestOptions.body = JSON.stringify(body);
    }
    return fetch(url, requestOptions).then(handleResponse);
  };
}

function requestWithCredentials(method, type) {
  return (url, body) => {
    const requestOptions = {
      method,
      credentials: 'include'
    };
    if (body) {
      requestOptions.headers['Content-Type'] = 'application/json';
      requestOptions.body = JSON.stringify(body);
    }
    return fetch(url, requestOptions).then(handleResponse);
  };
}

// helper functions

function authHeader(url, type = 'application/json') {
  // return auth header with jwt if user is logged in and request is to the api url
  const token = authToken();
  const isLoggedIn = !!token;
  const isApiUrl = url.includes('/api');
  if (isLoggedIn && isApiUrl) {
    return { Authorization: `Bearer ${token}`, Accept: 'application/json' };
  } else {
    return { Accept: type };
  }
}

function authToken() {
  return store.getState().auth.token;
}

function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);

    if (!response.ok) {
      if ([401, 403].includes(response.status) && authToken()) {
        // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
        // localStorage.removeItem('user');
        // const logout = () => store.dispatch(authActions.logout());
        // logout();
      }else if([500,501, 502, 503, 504].includes(response.status)){
        store.dispatch(authActions.error(true))
      }

      if ([301, 302, 307, 0].includes(response.status)) {
        // localStorage.removeItem('user');
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}
