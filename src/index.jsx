import React from 'react';
import { createRoot } from 'react-dom/client';

import { store } from './_store';
import { App } from './App';
import './i18n';

import { Provider } from 'react-redux';

const container = document.getElementById('oc-login-box');
const root = createRoot(container);

root.render(
  <Provider store={store}>
    <App />
  </Provider>
);
function onWidgetLoad() {
  const container = document.getElementById('oc-login-box');
  const root = createRoot(container);

  root.render(
    <Provider store={store}>
      <App />
    </Provider>
  );
}

/** Initialize widget once DOM has loaded. */
function onDomContentLoaded() {
  onWidgetLoad();
}

window.addEventListener('DOMContentLoaded', onDomContentLoaded);
